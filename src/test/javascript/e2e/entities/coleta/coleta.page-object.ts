import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class ColetaComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-coleta div table .btn-danger'));
  title = element.all(by.css('jhi-coleta div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ColetaUpdatePage {
  pageTitle = element(by.id('jhi-coleta-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  dataColetaInput = element(by.id('field_dataColeta'));
  unidadeMedidaInput = element(by.id('field_unidadeMedida'));
  valorCompraInput = element(by.id('field_valorCompra'));
  valorVendaInput = element(by.id('field_valorVenda'));
  revendaSelect = element(by.id('field_revenda'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDataColetaInput(dataColeta) {
    await this.dataColetaInput.sendKeys(dataColeta);
  }

  async getDataColetaInput() {
    return await this.dataColetaInput.getAttribute('value');
  }

  async setUnidadeMedidaInput(unidadeMedida) {
    await this.unidadeMedidaInput.sendKeys(unidadeMedida);
  }

  async getUnidadeMedidaInput() {
    return await this.unidadeMedidaInput.getAttribute('value');
  }

  async setValorCompraInput(valorCompra) {
    await this.valorCompraInput.sendKeys(valorCompra);
  }

  async getValorCompraInput() {
    return await this.valorCompraInput.getAttribute('value');
  }

  async setValorVendaInput(valorVenda) {
    await this.valorVendaInput.sendKeys(valorVenda);
  }

  async getValorVendaInput() {
    return await this.valorVendaInput.getAttribute('value');
  }

  async revendaSelectLastOption(timeout?: number) {
    await this.revendaSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async revendaSelectOption(option) {
    await this.revendaSelect.sendKeys(option);
  }

  getRevendaSelect(): ElementFinder {
    return this.revendaSelect;
  }

  async getRevendaSelectedOption() {
    return await this.revendaSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ColetaDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-coleta-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-coleta'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
