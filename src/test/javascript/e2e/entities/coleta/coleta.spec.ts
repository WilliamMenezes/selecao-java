/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ColetaComponentsPage, ColetaDeleteDialog, ColetaUpdatePage } from './coleta.page-object';

const expect = chai.expect;

describe('Coleta e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let coletaUpdatePage: ColetaUpdatePage;
  let coletaComponentsPage: ColetaComponentsPage;
  let coletaDeleteDialog: ColetaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Coletas', async () => {
    await navBarPage.goToEntity('coleta');
    coletaComponentsPage = new ColetaComponentsPage();
    await browser.wait(ec.visibilityOf(coletaComponentsPage.title), 5000);
    expect(await coletaComponentsPage.getTitle()).to.eq('indraApp.coleta.home.title');
  });

  it('should load create Coleta page', async () => {
    await coletaComponentsPage.clickOnCreateButton();
    coletaUpdatePage = new ColetaUpdatePage();
    expect(await coletaUpdatePage.getPageTitle()).to.eq('indraApp.coleta.home.createOrEditLabel');
    await coletaUpdatePage.cancel();
  });

  it('should create and save Coletas', async () => {
    const nbButtonsBeforeCreate = await coletaComponentsPage.countDeleteButtons();

    await coletaComponentsPage.clickOnCreateButton();
    await promise.all([
      coletaUpdatePage.setDataColetaInput('2000-12-31'),
      coletaUpdatePage.setUnidadeMedidaInput('unidadeMedida'),
      coletaUpdatePage.setValorCompraInput('5'),
      coletaUpdatePage.setValorVendaInput('5'),
      coletaUpdatePage.revendaSelectLastOption()
    ]);
    expect(await coletaUpdatePage.getDataColetaInput()).to.eq('2000-12-31', 'Expected dataColeta value to be equals to 2000-12-31');
    expect(await coletaUpdatePage.getUnidadeMedidaInput()).to.eq(
      'unidadeMedida',
      'Expected UnidadeMedida value to be equals to unidadeMedida'
    );
    expect(await coletaUpdatePage.getValorCompraInput()).to.eq('5', 'Expected valorCompra value to be equals to 5');
    expect(await coletaUpdatePage.getValorVendaInput()).to.eq('5', 'Expected valorVenda value to be equals to 5');
    await coletaUpdatePage.save();
    expect(await coletaUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await coletaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Coleta', async () => {
    const nbButtonsBeforeDelete = await coletaComponentsPage.countDeleteButtons();
    await coletaComponentsPage.clickOnLastDeleteButton();

    coletaDeleteDialog = new ColetaDeleteDialog();
    expect(await coletaDeleteDialog.getDialogTitle()).to.eq('indraApp.coleta.delete.question');
    await coletaDeleteDialog.clickOnConfirmButton();

    expect(await coletaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
