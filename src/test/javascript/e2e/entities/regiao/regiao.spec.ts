/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { RegiaoComponentsPage, RegiaoDeleteDialog, RegiaoUpdatePage } from './regiao.page-object';

const expect = chai.expect;

describe('Regiao e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let regiaoUpdatePage: RegiaoUpdatePage;
  let regiaoComponentsPage: RegiaoComponentsPage;
  let regiaoDeleteDialog: RegiaoDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Regiaos', async () => {
    await navBarPage.goToEntity('regiao');
    regiaoComponentsPage = new RegiaoComponentsPage();
    await browser.wait(ec.visibilityOf(regiaoComponentsPage.title), 5000);
    expect(await regiaoComponentsPage.getTitle()).to.eq('indraApp.regiao.home.title');
  });

  it('should load create Regiao page', async () => {
    await regiaoComponentsPage.clickOnCreateButton();
    regiaoUpdatePage = new RegiaoUpdatePage();
    expect(await regiaoUpdatePage.getPageTitle()).to.eq('indraApp.regiao.home.createOrEditLabel');
    await regiaoUpdatePage.cancel();
  });

  it('should create and save Regiaos', async () => {
    const nbButtonsBeforeCreate = await regiaoComponentsPage.countDeleteButtons();

    await regiaoComponentsPage.clickOnCreateButton();
    await promise.all([
      regiaoUpdatePage.setSiglaInput('sigla'),
      regiaoUpdatePage.setEstadoInput('estado'),
      regiaoUpdatePage.setMunicipioInput('municipio')
    ]);
    expect(await regiaoUpdatePage.getSiglaInput()).to.eq('sigla', 'Expected Sigla value to be equals to sigla');
    expect(await regiaoUpdatePage.getEstadoInput()).to.eq('estado', 'Expected Estado value to be equals to estado');
    expect(await regiaoUpdatePage.getMunicipioInput()).to.eq('municipio', 'Expected Municipio value to be equals to municipio');
    await regiaoUpdatePage.save();
    expect(await regiaoUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await regiaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Regiao', async () => {
    const nbButtonsBeforeDelete = await regiaoComponentsPage.countDeleteButtons();
    await regiaoComponentsPage.clickOnLastDeleteButton();

    regiaoDeleteDialog = new RegiaoDeleteDialog();
    expect(await regiaoDeleteDialog.getDialogTitle()).to.eq('indraApp.regiao.delete.question');
    await regiaoDeleteDialog.clickOnConfirmButton();

    expect(await regiaoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
