import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class RegiaoComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-regiao div table .btn-danger'));
  title = element.all(by.css('jhi-regiao div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class RegiaoUpdatePage {
  pageTitle = element(by.id('jhi-regiao-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  siglaInput = element(by.id('field_sigla'));
  estadoInput = element(by.id('field_estado'));
  municipioInput = element(by.id('field_municipio'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setSiglaInput(sigla) {
    await this.siglaInput.sendKeys(sigla);
  }

  async getSiglaInput() {
    return await this.siglaInput.getAttribute('value');
  }

  async setEstadoInput(estado) {
    await this.estadoInput.sendKeys(estado);
  }

  async getEstadoInput() {
    return await this.estadoInput.getAttribute('value');
  }

  async setMunicipioInput(municipio) {
    await this.municipioInput.sendKeys(municipio);
  }

  async getMunicipioInput() {
    return await this.municipioInput.getAttribute('value');
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class RegiaoDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-regiao-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-regiao'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
