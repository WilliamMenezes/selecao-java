import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class RevendaComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-revenda div table .btn-danger'));
  title = element.all(by.css('jhi-revenda div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class RevendaUpdatePage {
  pageTitle = element(by.id('jhi-revenda-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nomeInput = element(by.id('field_nome'));
  codigoRevendaInput = element(by.id('field_codigoRevenda'));
  bandeiraInput = element(by.id('field_bandeira'));
  produtoInput = element(by.id('field_produto'));
  regiaoSelect = element(by.id('field_regiao'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNomeInput(nome) {
    await this.nomeInput.sendKeys(nome);
  }

  async getNomeInput() {
    return await this.nomeInput.getAttribute('value');
  }

  async setCodigoRevendaInput(codigoRevenda) {
    await this.codigoRevendaInput.sendKeys(codigoRevenda);
  }

  async getCodigoRevendaInput() {
    return await this.codigoRevendaInput.getAttribute('value');
  }

  async setBandeiraInput(bandeira) {
    await this.bandeiraInput.sendKeys(bandeira);
  }

  async getBandeiraInput() {
    return await this.bandeiraInput.getAttribute('value');
  }

  async setProdutoInput(produto) {
    await this.produtoInput.sendKeys(produto);
  }

  async getProdutoInput() {
    return await this.produtoInput.getAttribute('value');
  }

  async regiaoSelectLastOption(timeout?: number) {
    await this.regiaoSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async regiaoSelectOption(option) {
    await this.regiaoSelect.sendKeys(option);
  }

  getRegiaoSelect(): ElementFinder {
    return this.regiaoSelect;
  }

  async getRegiaoSelectedOption() {
    return await this.regiaoSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class RevendaDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-revenda-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-revenda'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
