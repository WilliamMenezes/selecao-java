/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { RevendaComponentsPage, RevendaDeleteDialog, RevendaUpdatePage } from './revenda.page-object';

const expect = chai.expect;

describe('Revenda e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let revendaUpdatePage: RevendaUpdatePage;
  let revendaComponentsPage: RevendaComponentsPage;
  let revendaDeleteDialog: RevendaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Revendas', async () => {
    await navBarPage.goToEntity('revenda');
    revendaComponentsPage = new RevendaComponentsPage();
    await browser.wait(ec.visibilityOf(revendaComponentsPage.title), 5000);
    expect(await revendaComponentsPage.getTitle()).to.eq('indraApp.revenda.home.title');
  });

  it('should load create Revenda page', async () => {
    await revendaComponentsPage.clickOnCreateButton();
    revendaUpdatePage = new RevendaUpdatePage();
    expect(await revendaUpdatePage.getPageTitle()).to.eq('indraApp.revenda.home.createOrEditLabel');
    await revendaUpdatePage.cancel();
  });

  it('should create and save Revendas', async () => {
    const nbButtonsBeforeCreate = await revendaComponentsPage.countDeleteButtons();

    await revendaComponentsPage.clickOnCreateButton();
    await promise.all([
      revendaUpdatePage.setNomeInput('nome'),
      revendaUpdatePage.setCodigoRevendaInput('5'),
      revendaUpdatePage.setBandeiraInput('bandeira'),
      revendaUpdatePage.setProdutoInput('produto'),
      revendaUpdatePage.regiaoSelectLastOption()
    ]);
    expect(await revendaUpdatePage.getNomeInput()).to.eq('nome', 'Expected Nome value to be equals to nome');
    expect(await revendaUpdatePage.getCodigoRevendaInput()).to.eq('5', 'Expected codigoRevenda value to be equals to 5');
    expect(await revendaUpdatePage.getBandeiraInput()).to.eq('bandeira', 'Expected Bandeira value to be equals to bandeira');
    expect(await revendaUpdatePage.getProdutoInput()).to.eq('produto', 'Expected Produto value to be equals to produto');
    await revendaUpdatePage.save();
    expect(await revendaUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await revendaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Revenda', async () => {
    const nbButtonsBeforeDelete = await revendaComponentsPage.countDeleteButtons();
    await revendaComponentsPage.clickOnLastDeleteButton();

    revendaDeleteDialog = new RevendaDeleteDialog();
    expect(await revendaDeleteDialog.getDialogTitle()).to.eq('indraApp.revenda.delete.question');
    await revendaDeleteDialog.clickOnConfirmButton();

    expect(await revendaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
