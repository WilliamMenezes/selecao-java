/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { IndraTestModule } from '../../../test.module';
import { RevendaUpdateComponent } from 'app/entities/revenda/revenda-update.component';
import { RevendaService } from 'app/entities/revenda/revenda.service';
import { Revenda } from 'app/shared/model/revenda.model';

describe('Component Tests', () => {
  describe('Revenda Management Update Component', () => {
    let comp: RevendaUpdateComponent;
    let fixture: ComponentFixture<RevendaUpdateComponent>;
    let service: RevendaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IndraTestModule],
        declarations: [RevendaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RevendaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RevendaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RevendaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Revenda(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Revenda();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
