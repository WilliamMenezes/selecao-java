/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IndraTestModule } from '../../../test.module';
import { RevendaDetailComponent } from 'app/entities/revenda/revenda-detail.component';
import { Revenda } from 'app/shared/model/revenda.model';

describe('Component Tests', () => {
  describe('Revenda Management Detail Component', () => {
    let comp: RevendaDetailComponent;
    let fixture: ComponentFixture<RevendaDetailComponent>;
    const route = ({ data: of({ revenda: new Revenda(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IndraTestModule],
        declarations: [RevendaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RevendaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RevendaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.revenda).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
