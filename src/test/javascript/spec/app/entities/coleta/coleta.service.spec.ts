/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ColetaService } from 'app/entities/coleta/coleta.service';
import { IColeta, Coleta } from 'app/shared/model/coleta.model';

describe('Service Tests', () => {
  describe('Coleta Service', () => {
    let injector: TestBed;
    let service: ColetaService;
    let httpMock: HttpTestingController;
    let elemDefault: IColeta;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(ColetaService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Coleta(0, currentDate, 'AAAAAAA', 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            dataColeta: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Coleta', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dataColeta: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dataColeta: currentDate
          },
          returnedFromService
        );
        service
          .create(new Coleta(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Coleta', async () => {
        const returnedFromService = Object.assign(
          {
            dataColeta: currentDate.format(DATE_FORMAT),
            unidadeMedida: 'BBBBBB',
            valorCompra: 1,
            valorVenda: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dataColeta: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Coleta', async () => {
        const returnedFromService = Object.assign(
          {
            dataColeta: currentDate.format(DATE_FORMAT),
            unidadeMedida: 'BBBBBB',
            valorCompra: 1,
            valorVenda: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dataColeta: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Coleta', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
