/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IndraTestModule } from '../../../test.module';
import { ColetaDetailComponent } from 'app/entities/coleta/coleta-detail.component';
import { Coleta } from 'app/shared/model/coleta.model';

describe('Component Tests', () => {
  describe('Coleta Management Detail Component', () => {
    let comp: ColetaDetailComponent;
    let fixture: ComponentFixture<ColetaDetailComponent>;
    const route = ({ data: of({ coleta: new Coleta(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IndraTestModule],
        declarations: [ColetaDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ColetaDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ColetaDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.coleta).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
