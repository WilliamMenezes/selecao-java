/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { IndraTestModule } from '../../../test.module';
import { ColetaUpdateComponent } from 'app/entities/coleta/coleta-update.component';
import { ColetaService } from 'app/entities/coleta/coleta.service';
import { Coleta } from 'app/shared/model/coleta.model';

describe('Component Tests', () => {
  describe('Coleta Management Update Component', () => {
    let comp: ColetaUpdateComponent;
    let fixture: ComponentFixture<ColetaUpdateComponent>;
    let service: ColetaService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [IndraTestModule],
        declarations: [ColetaUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ColetaUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ColetaUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ColetaService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Coleta(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Coleta();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
