package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.IndraApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the MediaResource REST controller.
 *
 * @see MediaResource
 */
@SpringBootTest(classes = IndraApp.class)
public class MediaVMResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

//        MediaResource mediaResource = new MediaResource();
//        restMockMvc = MockMvcBuilders
//            .standaloneSetup(mediaResource)
//            .build();
    }

    /**
     * Test getMedia
     */
    @Test
    public void testGetMedia() throws Exception {
        restMockMvc.perform(get("/api/media/get-media"))
            .andExpect(status().isOk());
    }

    /**
     * Test findMedia
     */
    @Test
    public void testFindMedia() throws Exception {
        restMockMvc.perform(post("/api/media/find-media"))
            .andExpect(status().isOk());
    }
}
