package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.IndraApp;
import com.indracompany.selecao.domain.Coleta;
import com.indracompany.selecao.repository.ColetaRepository;
import com.indracompany.selecao.service.ColetaService;
import com.indracompany.selecao.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.indracompany.selecao.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ColetaResource} REST controller.
 */
@SpringBootTest(classes = IndraApp.class)
public class ColetaResourceIT {

    private static final LocalDate DEFAULT_DATA_COLETA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_COLETA = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_UNIDADE_MEDIDA = "AAAAAAAAAA";
    private static final String UPDATED_UNIDADE_MEDIDA = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_VALOR_COMPRA = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR_COMPRA = new BigDecimal(2);

    private static final BigDecimal DEFAULT_VALOR_VENDA = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALOR_VENDA = new BigDecimal(2);

    @Autowired
    private ColetaRepository coletaRepository;

    @Autowired
    private ColetaService coletaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restColetaMockMvc;

    private Coleta coleta;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ColetaResource coletaResource = new ColetaResource(coletaService);
        this.restColetaMockMvc = MockMvcBuilders.standaloneSetup(coletaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coleta createEntity(EntityManager em) {
        Coleta coleta = new Coleta()
            .dataColeta(DEFAULT_DATA_COLETA)
            .unidadeMedida(DEFAULT_UNIDADE_MEDIDA)
            .valorCompra(DEFAULT_VALOR_COMPRA)
            .valorVenda(DEFAULT_VALOR_VENDA);
        return coleta;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coleta createUpdatedEntity(EntityManager em) {
        Coleta coleta = new Coleta()
            .dataColeta(UPDATED_DATA_COLETA)
            .unidadeMedida(UPDATED_UNIDADE_MEDIDA)
            .valorCompra(UPDATED_VALOR_COMPRA)
            .valorVenda(UPDATED_VALOR_VENDA);
        return coleta;
    }

    @BeforeEach
    public void initTest() {
        coleta = createEntity(em);
    }

    @Test
    @Transactional
    public void createColeta() throws Exception {
        int databaseSizeBeforeCreate = coletaRepository.findAll().size();

        // Create the Coleta
        restColetaMockMvc.perform(post("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isCreated());

        // Validate the Coleta in the database
        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeCreate + 1);
        Coleta testColeta = coletaList.get(coletaList.size() - 1);
        assertThat(testColeta.getDataColeta()).isEqualTo(DEFAULT_DATA_COLETA);
        assertThat(testColeta.getUnidadeMedida()).isEqualTo(DEFAULT_UNIDADE_MEDIDA);
        assertThat(testColeta.getValorCompra()).isEqualTo(DEFAULT_VALOR_COMPRA);
        assertThat(testColeta.getValorVenda()).isEqualTo(DEFAULT_VALOR_VENDA);
    }

    @Test
    @Transactional
    public void createColetaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = coletaRepository.findAll().size();

        // Create the Coleta with an existing ID
        coleta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restColetaMockMvc.perform(post("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isBadRequest());

        // Validate the Coleta in the database
        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDataColetaIsRequired() throws Exception {
        int databaseSizeBeforeTest = coletaRepository.findAll().size();
        // set the field null
        coleta.setDataColeta(null);

        // Create the Coleta, which fails.

        restColetaMockMvc.perform(post("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isBadRequest());

        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnidadeMedidaIsRequired() throws Exception {
        int databaseSizeBeforeTest = coletaRepository.findAll().size();
        // set the field null
        coleta.setUnidadeMedida(null);

        // Create the Coleta, which fails.

        restColetaMockMvc.perform(post("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isBadRequest());

        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorCompraIsRequired() throws Exception {
        int databaseSizeBeforeTest = coletaRepository.findAll().size();
        // set the field null
        coleta.setValorCompra(null);

        // Create the Coleta, which fails.

        restColetaMockMvc.perform(post("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isBadRequest());

        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorVendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = coletaRepository.findAll().size();
        // set the field null
        coleta.setValorVenda(null);

        // Create the Coleta, which fails.

        restColetaMockMvc.perform(post("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isBadRequest());

        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllColetas() throws Exception {
        // Initialize the database
        coletaRepository.saveAndFlush(coleta);

        // Get all the coletaList
        restColetaMockMvc.perform(get("/api/coletas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coleta.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataColeta").value(hasItem(DEFAULT_DATA_COLETA.toString())))
            .andExpect(jsonPath("$.[*].unidadeMedida").value(hasItem(DEFAULT_UNIDADE_MEDIDA.toString())))
            .andExpect(jsonPath("$.[*].valorCompra").value(hasItem(DEFAULT_VALOR_COMPRA.intValue())))
            .andExpect(jsonPath("$.[*].valorVenda").value(hasItem(DEFAULT_VALOR_VENDA.intValue())));
    }
    
    @Test
    @Transactional
    public void getColeta() throws Exception {
        // Initialize the database
        coletaRepository.saveAndFlush(coleta);

        // Get the coleta
        restColetaMockMvc.perform(get("/api/coletas/{id}", coleta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(coleta.getId().intValue()))
            .andExpect(jsonPath("$.dataColeta").value(DEFAULT_DATA_COLETA.toString()))
            .andExpect(jsonPath("$.unidadeMedida").value(DEFAULT_UNIDADE_MEDIDA.toString()))
            .andExpect(jsonPath("$.valorCompra").value(DEFAULT_VALOR_COMPRA.intValue()))
            .andExpect(jsonPath("$.valorVenda").value(DEFAULT_VALOR_VENDA.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingColeta() throws Exception {
        // Get the coleta
        restColetaMockMvc.perform(get("/api/coletas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateColeta() throws Exception {
        // Initialize the database
        coletaService.save(coleta);

        int databaseSizeBeforeUpdate = coletaRepository.findAll().size();

        // Update the coleta
        Coleta updatedColeta = coletaRepository.findById(coleta.getId()).get();
        // Disconnect from session so that the updates on updatedColeta are not directly saved in db
        em.detach(updatedColeta);
        updatedColeta
            .dataColeta(UPDATED_DATA_COLETA)
            .unidadeMedida(UPDATED_UNIDADE_MEDIDA)
            .valorCompra(UPDATED_VALOR_COMPRA)
            .valorVenda(UPDATED_VALOR_VENDA);

        restColetaMockMvc.perform(put("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedColeta)))
            .andExpect(status().isOk());

        // Validate the Coleta in the database
        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeUpdate);
        Coleta testColeta = coletaList.get(coletaList.size() - 1);
        assertThat(testColeta.getDataColeta()).isEqualTo(UPDATED_DATA_COLETA);
        assertThat(testColeta.getUnidadeMedida()).isEqualTo(UPDATED_UNIDADE_MEDIDA);
        assertThat(testColeta.getValorCompra()).isEqualTo(UPDATED_VALOR_COMPRA);
        assertThat(testColeta.getValorVenda()).isEqualTo(UPDATED_VALOR_VENDA);
    }

    @Test
    @Transactional
    public void updateNonExistingColeta() throws Exception {
        int databaseSizeBeforeUpdate = coletaRepository.findAll().size();

        // Create the Coleta

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restColetaMockMvc.perform(put("/api/coletas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(coleta)))
            .andExpect(status().isBadRequest());

        // Validate the Coleta in the database
        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteColeta() throws Exception {
        // Initialize the database
        coletaService.save(coleta);

        int databaseSizeBeforeDelete = coletaRepository.findAll().size();

        // Delete the coleta
        restColetaMockMvc.perform(delete("/api/coletas/{id}", coleta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Coleta> coletaList = coletaRepository.findAll();
        assertThat(coletaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Coleta.class);
        Coleta coleta1 = new Coleta();
        coleta1.setId(1L);
        Coleta coleta2 = new Coleta();
        coleta2.setId(coleta1.getId());
        assertThat(coleta1).isEqualTo(coleta2);
        coleta2.setId(2L);
        assertThat(coleta1).isNotEqualTo(coleta2);
        coleta1.setId(null);
        assertThat(coleta1).isNotEqualTo(coleta2);
    }
}
