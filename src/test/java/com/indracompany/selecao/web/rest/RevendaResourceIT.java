package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.IndraApp;
import com.indracompany.selecao.domain.Revenda;
import com.indracompany.selecao.repository.RevendaRepository;
import com.indracompany.selecao.service.RevendaService;
import com.indracompany.selecao.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.indracompany.selecao.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link RevendaResource} REST controller.
 */
@SpringBootTest(classes = IndraApp.class)
public class RevendaResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODIGO_REVENDA = 1;
    private static final Integer UPDATED_CODIGO_REVENDA = 2;

    private static final String DEFAULT_BANDEIRA = "AAAAAAAAAA";
    private static final String UPDATED_BANDEIRA = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUTO = "AAAAAAAAAA";
    private static final String UPDATED_PRODUTO = "BBBBBBBBBB";

    @Autowired
    private RevendaRepository revendaRepository;

    @Autowired
    private RevendaService revendaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRevendaMockMvc;

    private Revenda revenda;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RevendaResource revendaResource = new RevendaResource(revendaService);
        this.restRevendaMockMvc = MockMvcBuilders.standaloneSetup(revendaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Revenda createEntity(EntityManager em) {
        Revenda revenda = new Revenda()
            .nome(DEFAULT_NOME)
            .codigoRevenda(DEFAULT_CODIGO_REVENDA)
            .bandeira(DEFAULT_BANDEIRA)
            .produto(DEFAULT_PRODUTO);
        return revenda;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Revenda createUpdatedEntity(EntityManager em) {
        Revenda revenda = new Revenda()
            .nome(UPDATED_NOME)
            .codigoRevenda(UPDATED_CODIGO_REVENDA)
            .bandeira(UPDATED_BANDEIRA)
            .produto(UPDATED_PRODUTO);
        return revenda;
    }

    @BeforeEach
    public void initTest() {
        revenda = createEntity(em);
    }

    @Test
    @Transactional
    public void createRevenda() throws Exception {
        int databaseSizeBeforeCreate = revendaRepository.findAll().size();

        // Create the Revenda
        restRevendaMockMvc.perform(post("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isCreated());

        // Validate the Revenda in the database
        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeCreate + 1);
        Revenda testRevenda = revendaList.get(revendaList.size() - 1);
        assertThat(testRevenda.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testRevenda.getCodigoRevenda()).isEqualTo(DEFAULT_CODIGO_REVENDA);
        assertThat(testRevenda.getBandeira()).isEqualTo(DEFAULT_BANDEIRA);
        assertThat(testRevenda.getProduto()).isEqualTo(DEFAULT_PRODUTO);
    }

    @Test
    @Transactional
    public void createRevendaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = revendaRepository.findAll().size();

        // Create the Revenda with an existing ID
        revenda.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRevendaMockMvc.perform(post("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isBadRequest());

        // Validate the Revenda in the database
        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = revendaRepository.findAll().size();
        // set the field null
        revenda.setNome(null);

        // Create the Revenda, which fails.

        restRevendaMockMvc.perform(post("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isBadRequest());

        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodigoRevendaIsRequired() throws Exception {
        int databaseSizeBeforeTest = revendaRepository.findAll().size();
        // set the field null
        revenda.setCodigoRevenda(null);

        // Create the Revenda, which fails.

        restRevendaMockMvc.perform(post("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isBadRequest());

        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBandeiraIsRequired() throws Exception {
        int databaseSizeBeforeTest = revendaRepository.findAll().size();
        // set the field null
        revenda.setBandeira(null);

        // Create the Revenda, which fails.

        restRevendaMockMvc.perform(post("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isBadRequest());

        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProdutoIsRequired() throws Exception {
        int databaseSizeBeforeTest = revendaRepository.findAll().size();
        // set the field null
        revenda.setProduto(null);

        // Create the Revenda, which fails.

        restRevendaMockMvc.perform(post("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isBadRequest());

        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRevendas() throws Exception {
        // Initialize the database
        revendaRepository.saveAndFlush(revenda);

        // Get all the revendaList
        restRevendaMockMvc.perform(get("/api/revendas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(revenda.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].codigoRevenda").value(hasItem(DEFAULT_CODIGO_REVENDA)))
            .andExpect(jsonPath("$.[*].bandeira").value(hasItem(DEFAULT_BANDEIRA.toString())))
            .andExpect(jsonPath("$.[*].produto").value(hasItem(DEFAULT_PRODUTO.toString())));
    }
    
    @Test
    @Transactional
    public void getRevenda() throws Exception {
        // Initialize the database
        revendaRepository.saveAndFlush(revenda);

        // Get the revenda
        restRevendaMockMvc.perform(get("/api/revendas/{id}", revenda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(revenda.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.codigoRevenda").value(DEFAULT_CODIGO_REVENDA))
            .andExpect(jsonPath("$.bandeira").value(DEFAULT_BANDEIRA.toString()))
            .andExpect(jsonPath("$.produto").value(DEFAULT_PRODUTO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRevenda() throws Exception {
        // Get the revenda
        restRevendaMockMvc.perform(get("/api/revendas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRevenda() throws Exception {
        // Initialize the database
        revendaService.save(revenda);

        int databaseSizeBeforeUpdate = revendaRepository.findAll().size();

        // Update the revenda
        Revenda updatedRevenda = revendaRepository.findById(revenda.getId()).get();
        // Disconnect from session so that the updates on updatedRevenda are not directly saved in db
        em.detach(updatedRevenda);
        updatedRevenda
            .nome(UPDATED_NOME)
            .codigoRevenda(UPDATED_CODIGO_REVENDA)
            .bandeira(UPDATED_BANDEIRA)
            .produto(UPDATED_PRODUTO);

        restRevendaMockMvc.perform(put("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRevenda)))
            .andExpect(status().isOk());

        // Validate the Revenda in the database
        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeUpdate);
        Revenda testRevenda = revendaList.get(revendaList.size() - 1);
        assertThat(testRevenda.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testRevenda.getCodigoRevenda()).isEqualTo(UPDATED_CODIGO_REVENDA);
        assertThat(testRevenda.getBandeira()).isEqualTo(UPDATED_BANDEIRA);
        assertThat(testRevenda.getProduto()).isEqualTo(UPDATED_PRODUTO);
    }

    @Test
    @Transactional
    public void updateNonExistingRevenda() throws Exception {
        int databaseSizeBeforeUpdate = revendaRepository.findAll().size();

        // Create the Revenda

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRevendaMockMvc.perform(put("/api/revendas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(revenda)))
            .andExpect(status().isBadRequest());

        // Validate the Revenda in the database
        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRevenda() throws Exception {
        // Initialize the database
        revendaService.save(revenda);

        int databaseSizeBeforeDelete = revendaRepository.findAll().size();

        // Delete the revenda
        restRevendaMockMvc.perform(delete("/api/revendas/{id}", revenda.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Revenda> revendaList = revendaRepository.findAll();
        assertThat(revendaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Revenda.class);
        Revenda revenda1 = new Revenda();
        revenda1.setId(1L);
        Revenda revenda2 = new Revenda();
        revenda2.setId(revenda1.getId());
        assertThat(revenda1).isEqualTo(revenda2);
        revenda2.setId(2L);
        assertThat(revenda1).isNotEqualTo(revenda2);
        revenda1.setId(null);
        assertThat(revenda1).isNotEqualTo(revenda2);
    }
}
