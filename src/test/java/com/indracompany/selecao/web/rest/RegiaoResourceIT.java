package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.IndraApp;
import com.indracompany.selecao.domain.Regiao;
import com.indracompany.selecao.repository.RegiaoRepository;
import com.indracompany.selecao.service.RegiaoService;
import com.indracompany.selecao.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.indracompany.selecao.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link RegiaoResource} REST controller.
 */
@SpringBootTest(classes = IndraApp.class)
public class RegiaoResourceIT {

    private static final String DEFAULT_SIGLA = "AA";
    private static final String UPDATED_SIGLA = "BB";

    private static final String DEFAULT_ESTADO = "AAAAAAAAAA";
    private static final String UPDATED_ESTADO = "BBBBBBBBBB";

    private static final String DEFAULT_MUNICIPIO = "AAAAAAAAAA";
    private static final String UPDATED_MUNICIPIO = "BBBBBBBBBB";

    @Autowired
    private RegiaoRepository regiaoRepository;

    @Autowired
    private RegiaoService regiaoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRegiaoMockMvc;

    private Regiao regiao;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegiaoResource regiaoResource = new RegiaoResource(regiaoService);
        this.restRegiaoMockMvc = MockMvcBuilders.standaloneSetup(regiaoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Regiao createEntity(EntityManager em) {
        Regiao regiao = new Regiao()
            .sigla(DEFAULT_SIGLA)
            .estado(DEFAULT_ESTADO)
            .municipio(DEFAULT_MUNICIPIO);
        return regiao;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Regiao createUpdatedEntity(EntityManager em) {
        Regiao regiao = new Regiao()
            .sigla(UPDATED_SIGLA)
            .estado(UPDATED_ESTADO)
            .municipio(UPDATED_MUNICIPIO);
        return regiao;
    }

    @BeforeEach
    public void initTest() {
        regiao = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegiao() throws Exception {
        int databaseSizeBeforeCreate = regiaoRepository.findAll().size();

        // Create the Regiao
        restRegiaoMockMvc.perform(post("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regiao)))
            .andExpect(status().isCreated());

        // Validate the Regiao in the database
        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeCreate + 1);
        Regiao testRegiao = regiaoList.get(regiaoList.size() - 1);
        assertThat(testRegiao.getSigla()).isEqualTo(DEFAULT_SIGLA);
        assertThat(testRegiao.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testRegiao.getMunicipio()).isEqualTo(DEFAULT_MUNICIPIO);
    }

    @Test
    @Transactional
    public void createRegiaoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = regiaoRepository.findAll().size();

        // Create the Regiao with an existing ID
        regiao.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegiaoMockMvc.perform(post("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regiao)))
            .andExpect(status().isBadRequest());

        // Validate the Regiao in the database
        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSiglaIsRequired() throws Exception {
        int databaseSizeBeforeTest = regiaoRepository.findAll().size();
        // set the field null
        regiao.setSigla(null);

        // Create the Regiao, which fails.

        restRegiaoMockMvc.perform(post("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regiao)))
            .andExpect(status().isBadRequest());

        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = regiaoRepository.findAll().size();
        // set the field null
        regiao.setEstado(null);

        // Create the Regiao, which fails.

        restRegiaoMockMvc.perform(post("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regiao)))
            .andExpect(status().isBadRequest());

        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMunicipioIsRequired() throws Exception {
        int databaseSizeBeforeTest = regiaoRepository.findAll().size();
        // set the field null
        regiao.setMunicipio(null);

        // Create the Regiao, which fails.

        restRegiaoMockMvc.perform(post("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regiao)))
            .andExpect(status().isBadRequest());

        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegiaos() throws Exception {
        // Initialize the database
        regiaoRepository.saveAndFlush(regiao);

        // Get all the regiaoList
        restRegiaoMockMvc.perform(get("/api/regiaos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(regiao.getId().intValue())))
            .andExpect(jsonPath("$.[*].sigla").value(hasItem(DEFAULT_SIGLA.toString())))
            .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.toString())))
            .andExpect(jsonPath("$.[*].municipio").value(hasItem(DEFAULT_MUNICIPIO.toString())));
    }
    
    @Test
    @Transactional
    public void getRegiao() throws Exception {
        // Initialize the database
        regiaoRepository.saveAndFlush(regiao);

        // Get the regiao
        restRegiaoMockMvc.perform(get("/api/regiaos/{id}", regiao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(regiao.getId().intValue()))
            .andExpect(jsonPath("$.sigla").value(DEFAULT_SIGLA.toString()))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.toString()))
            .andExpect(jsonPath("$.municipio").value(DEFAULT_MUNICIPIO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRegiao() throws Exception {
        // Get the regiao
        restRegiaoMockMvc.perform(get("/api/regiaos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegiao() throws Exception {
        // Initialize the database
        regiaoService.save(regiao);

        int databaseSizeBeforeUpdate = regiaoRepository.findAll().size();

        // Update the regiao
        Regiao updatedRegiao = regiaoRepository.findById(regiao.getId()).get();
        // Disconnect from session so that the updates on updatedRegiao are not directly saved in db
        em.detach(updatedRegiao);
        updatedRegiao
            .sigla(UPDATED_SIGLA)
            .estado(UPDATED_ESTADO)
            .municipio(UPDATED_MUNICIPIO);

        restRegiaoMockMvc.perform(put("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegiao)))
            .andExpect(status().isOk());

        // Validate the Regiao in the database
        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeUpdate);
        Regiao testRegiao = regiaoList.get(regiaoList.size() - 1);
        assertThat(testRegiao.getSigla()).isEqualTo(UPDATED_SIGLA);
        assertThat(testRegiao.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testRegiao.getMunicipio()).isEqualTo(UPDATED_MUNICIPIO);
    }

    @Test
    @Transactional
    public void updateNonExistingRegiao() throws Exception {
        int databaseSizeBeforeUpdate = regiaoRepository.findAll().size();

        // Create the Regiao

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegiaoMockMvc.perform(put("/api/regiaos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(regiao)))
            .andExpect(status().isBadRequest());

        // Validate the Regiao in the database
        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRegiao() throws Exception {
        // Initialize the database
        regiaoService.save(regiao);

        int databaseSizeBeforeDelete = regiaoRepository.findAll().size();

        // Delete the regiao
        restRegiaoMockMvc.perform(delete("/api/regiaos/{id}", regiao.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Regiao> regiaoList = regiaoRepository.findAll();
        assertThat(regiaoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Regiao.class);
        Regiao regiao1 = new Regiao();
        regiao1.setId(1L);
        Regiao regiao2 = new Regiao();
        regiao2.setId(regiao1.getId());
        assertThat(regiao1).isEqualTo(regiao2);
        regiao2.setId(2L);
        assertThat(regiao1).isNotEqualTo(regiao2);
        regiao1.setId(null);
        assertThat(regiao1).isNotEqualTo(regiao2);
    }
}
