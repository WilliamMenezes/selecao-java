import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRevenda } from 'app/shared/model/revenda.model';
import { RevendaService } from './revenda.service';

@Component({
  selector: 'jhi-revenda-delete-dialog',
  templateUrl: './revenda-delete-dialog.component.html'
})
export class RevendaDeleteDialogComponent {
  revenda: IRevenda;

  constructor(protected revendaService: RevendaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.revendaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'revendaListModification',
        content: 'Deleted an revenda'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-revenda-delete-popup',
  template: ''
})
export class RevendaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ revenda }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RevendaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.revenda = revenda;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/revenda', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/revenda', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
