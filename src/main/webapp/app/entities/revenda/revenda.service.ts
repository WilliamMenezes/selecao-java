import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRevenda } from 'app/shared/model/revenda.model';

type EntityResponseType = HttpResponse<IRevenda>;
type EntityArrayResponseType = HttpResponse<IRevenda[]>;

@Injectable({ providedIn: 'root' })
export class RevendaService {
  public resourceUrl = SERVER_API_URL + 'api/revendas';

  constructor(protected http: HttpClient) {}

  create(revenda: IRevenda): Observable<EntityResponseType> {
    return this.http.post<IRevenda>(this.resourceUrl, revenda, { observe: 'response' });
  }

  update(revenda: IRevenda): Observable<EntityResponseType> {
    return this.http.put<IRevenda>(this.resourceUrl, revenda, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRevenda>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRevenda[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
