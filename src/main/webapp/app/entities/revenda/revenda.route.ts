import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Revenda } from 'app/shared/model/revenda.model';
import { RevendaService } from './revenda.service';
import { RevendaComponent } from './revenda.component';
import { RevendaDetailComponent } from './revenda-detail.component';
import { RevendaUpdateComponent } from './revenda-update.component';
import { RevendaDeletePopupComponent } from './revenda-delete-dialog.component';
import { IRevenda } from 'app/shared/model/revenda.model';

@Injectable({ providedIn: 'root' })
export class RevendaResolve implements Resolve<IRevenda> {
  constructor(private service: RevendaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRevenda> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Revenda>) => response.ok),
        map((revenda: HttpResponse<Revenda>) => revenda.body)
      );
    }
    return of(new Revenda());
  }
}

export const revendaRoute: Routes = [
  {
    path: '',
    component: RevendaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'indraApp.revenda.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RevendaDetailComponent,
    resolve: {
      revenda: RevendaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.revenda.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RevendaUpdateComponent,
    resolve: {
      revenda: RevendaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.revenda.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RevendaUpdateComponent,
    resolve: {
      revenda: RevendaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.revenda.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const revendaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RevendaDeletePopupComponent,
    resolve: {
      revenda: RevendaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.revenda.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
