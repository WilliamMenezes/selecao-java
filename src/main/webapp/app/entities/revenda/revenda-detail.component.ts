import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRevenda } from 'app/shared/model/revenda.model';

@Component({
  selector: 'jhi-revenda-detail',
  templateUrl: './revenda-detail.component.html'
})
export class RevendaDetailComponent implements OnInit {
  revenda: IRevenda;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ revenda }) => {
      this.revenda = revenda;
    });
  }

  previousState() {
    window.history.back();
  }
}
