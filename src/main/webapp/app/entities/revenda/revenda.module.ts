import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { IndraSharedModule } from 'app/shared';
import {
  RevendaComponent,
  RevendaDetailComponent,
  RevendaUpdateComponent,
  RevendaDeletePopupComponent,
  RevendaDeleteDialogComponent,
  revendaRoute,
  revendaPopupRoute
} from './';

const ENTITY_STATES = [...revendaRoute, ...revendaPopupRoute];

@NgModule({
  imports: [IndraSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RevendaComponent,
    RevendaDetailComponent,
    RevendaUpdateComponent,
    RevendaDeleteDialogComponent,
    RevendaDeletePopupComponent
  ],
  entryComponents: [RevendaComponent, RevendaUpdateComponent, RevendaDeleteDialogComponent, RevendaDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IndraRevendaModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
