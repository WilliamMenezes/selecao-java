import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IRevenda, Revenda } from 'app/shared/model/revenda.model';
import { RevendaService } from './revenda.service';
import { IRegiao } from 'app/shared/model/regiao.model';
import { RegiaoService } from 'app/entities/regiao';

@Component({
  selector: 'jhi-revenda-update',
  templateUrl: './revenda-update.component.html'
})
export class RevendaUpdateComponent implements OnInit {
  revenda: IRevenda;
  isSaving: boolean;

  regiaos: IRegiao[];

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    codigoRevenda: [null, [Validators.required]],
    bandeira: [null, [Validators.required]],
    produto: [null, [Validators.required]],
    regiao: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected revendaService: RevendaService,
    protected regiaoService: RegiaoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ revenda }) => {
      this.updateForm(revenda);
      this.revenda = revenda;
    });
    this.regiaoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRegiao[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRegiao[]>) => response.body)
      )
      .subscribe((res: IRegiao[]) => (this.regiaos = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(revenda: IRevenda) {
    this.editForm.patchValue({
      id: revenda.id,
      nome: revenda.nome,
      codigoRevenda: revenda.codigoRevenda,
      bandeira: revenda.bandeira,
      produto: revenda.produto,
      regiao: revenda.regiao
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const revenda = this.createFromForm();
    if (revenda.id !== undefined) {
      this.subscribeToSaveResponse(this.revendaService.update(revenda));
    } else {
      this.subscribeToSaveResponse(this.revendaService.create(revenda));
    }
  }

  private createFromForm(): IRevenda {
    const entity = {
      ...new Revenda(),
      id: this.editForm.get(['id']).value,
      nome: this.editForm.get(['nome']).value,
      codigoRevenda: this.editForm.get(['codigoRevenda']).value,
      bandeira: this.editForm.get(['bandeira']).value,
      produto: this.editForm.get(['produto']).value,
      regiao: this.editForm.get(['regiao']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRevenda>>) {
    result.subscribe((res: HttpResponse<IRevenda>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRegiaoById(index: number, item: IRegiao) {
    return item.id;
  }
}
