export * from './revenda.service';
export * from './revenda-update.component';
export * from './revenda-delete-dialog.component';
export * from './revenda-detail.component';
export * from './revenda.component';
export * from './revenda.route';
