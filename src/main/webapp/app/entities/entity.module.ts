import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'regiao',
        loadChildren: './regiao/regiao.module#IndraRegiaoModule'
      },
      {
        path: 'revenda',
        loadChildren: './revenda/revenda.module#IndraRevendaModule'
      },
      {
        path: 'coleta',
        loadChildren: './coleta/coleta.module#IndraColetaModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IndraEntityModule {}
