import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRegiao } from 'app/shared/model/regiao.model';
import { RegiaoService } from './regiao.service';

@Component({
  selector: 'jhi-regiao-delete-dialog',
  templateUrl: './regiao-delete-dialog.component.html'
})
export class RegiaoDeleteDialogComponent {
  regiao: IRegiao;

  constructor(protected regiaoService: RegiaoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.regiaoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'regiaoListModification',
        content: 'Deleted an regiao'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-regiao-delete-popup',
  template: ''
})
export class RegiaoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ regiao }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RegiaoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.regiao = regiao;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/regiao', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/regiao', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
