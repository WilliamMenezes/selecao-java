import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Regiao } from 'app/shared/model/regiao.model';
import { RegiaoService } from './regiao.service';
import { RegiaoComponent } from './regiao.component';
import { RegiaoDetailComponent } from './regiao-detail.component';
import { RegiaoUpdateComponent } from './regiao-update.component';
import { RegiaoDeletePopupComponent } from './regiao-delete-dialog.component';
import { IRegiao } from 'app/shared/model/regiao.model';

@Injectable({ providedIn: 'root' })
export class RegiaoResolve implements Resolve<IRegiao> {
  constructor(private service: RegiaoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRegiao> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Regiao>) => response.ok),
        map((regiao: HttpResponse<Regiao>) => regiao.body)
      );
    }
    return of(new Regiao());
  }
}

export const regiaoRoute: Routes = [
  {
    path: '',
    component: RegiaoComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'indraApp.regiao.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RegiaoDetailComponent,
    resolve: {
      regiao: RegiaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.regiao.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RegiaoUpdateComponent,
    resolve: {
      regiao: RegiaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.regiao.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RegiaoUpdateComponent,
    resolve: {
      regiao: RegiaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.regiao.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const regiaoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RegiaoDeletePopupComponent,
    resolve: {
      regiao: RegiaoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.regiao.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
