import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { IndraSharedModule } from 'app/shared';
import {
  RegiaoComponent,
  RegiaoDetailComponent,
  RegiaoUpdateComponent,
  RegiaoDeletePopupComponent,
  RegiaoDeleteDialogComponent,
  regiaoRoute,
  regiaoPopupRoute
} from './';

const ENTITY_STATES = [...regiaoRoute, ...regiaoPopupRoute];

@NgModule({
  imports: [IndraSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [RegiaoComponent, RegiaoDetailComponent, RegiaoUpdateComponent, RegiaoDeleteDialogComponent, RegiaoDeletePopupComponent],
  entryComponents: [RegiaoComponent, RegiaoUpdateComponent, RegiaoDeleteDialogComponent, RegiaoDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IndraRegiaoModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
