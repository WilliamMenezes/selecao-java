import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IRegiao, Regiao } from 'app/shared/model/regiao.model';
import { RegiaoService } from './regiao.service';

@Component({
  selector: 'jhi-regiao-update',
  templateUrl: './regiao-update.component.html'
})
export class RegiaoUpdateComponent implements OnInit {
  regiao: IRegiao;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    sigla: [null, [Validators.required, Validators.maxLength(2)]],
    estado: [null, [Validators.required, Validators.maxLength(20)]],
    municipio: [null, [Validators.required, Validators.maxLength(50)]]
  });

  constructor(protected regiaoService: RegiaoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ regiao }) => {
      this.updateForm(regiao);
      this.regiao = regiao;
    });
  }

  updateForm(regiao: IRegiao) {
    this.editForm.patchValue({
      id: regiao.id,
      sigla: regiao.sigla,
      estado: regiao.estado,
      municipio: regiao.municipio
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const regiao = this.createFromForm();
    if (regiao.id !== undefined) {
      this.subscribeToSaveResponse(this.regiaoService.update(regiao));
    } else {
      this.subscribeToSaveResponse(this.regiaoService.create(regiao));
    }
  }

  private createFromForm(): IRegiao {
    const entity = {
      ...new Regiao(),
      id: this.editForm.get(['id']).value,
      sigla: this.editForm.get(['sigla']).value,
      estado: this.editForm.get(['estado']).value,
      municipio: this.editForm.get(['municipio']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRegiao>>) {
    result.subscribe((res: HttpResponse<IRegiao>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
