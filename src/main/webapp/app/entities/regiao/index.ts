export * from './regiao.service';
export * from './regiao-update.component';
export * from './regiao-delete-dialog.component';
export * from './regiao-detail.component';
export * from './regiao.component';
export * from './regiao.route';
