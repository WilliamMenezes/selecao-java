import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IColeta } from 'app/shared/model/coleta.model';

@Component({
  selector: 'jhi-coleta-detail',
  templateUrl: './coleta-detail.component.html'
})
export class ColetaDetailComponent implements OnInit {
  coleta: IColeta;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ coleta }) => {
      this.coleta = coleta;
    });
  }

  previousState() {
    window.history.back();
  }
}
