import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IColeta } from 'app/shared/model/coleta.model';
import { ColetaService } from './coleta.service';

@Component({
  selector: 'jhi-coleta-delete-dialog',
  templateUrl: './coleta-delete-dialog.component.html'
})
export class ColetaDeleteDialogComponent {
  coleta: IColeta;

  constructor(protected coletaService: ColetaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.coletaService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'coletaListModification',
        content: 'Deleted an coleta'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-coleta-delete-popup',
  template: ''
})
export class ColetaDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ coleta }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ColetaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.coleta = coleta;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/coleta', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/coleta', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
