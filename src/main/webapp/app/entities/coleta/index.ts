export * from './coleta.service';
export * from './coleta-update.component';
export * from './coleta-delete-dialog.component';
export * from './coleta-detail.component';
export * from './coleta.component';
export * from './coleta.route';
