import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { IndraSharedModule } from 'app/shared';
import {
  ColetaComponent,
  ColetaDetailComponent,
  ColetaUpdateComponent,
  ColetaDeletePopupComponent,
  ColetaDeleteDialogComponent,
  coletaRoute,
  coletaPopupRoute
} from './';

const ENTITY_STATES = [...coletaRoute, ...coletaPopupRoute];

@NgModule({
  imports: [IndraSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ColetaComponent, ColetaDetailComponent, ColetaUpdateComponent, ColetaDeleteDialogComponent, ColetaDeletePopupComponent],
  entryComponents: [ColetaComponent, ColetaUpdateComponent, ColetaDeleteDialogComponent, ColetaDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IndraColetaModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
