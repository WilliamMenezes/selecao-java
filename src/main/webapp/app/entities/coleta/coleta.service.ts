import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IColeta } from 'app/shared/model/coleta.model';

type EntityResponseType = HttpResponse<IColeta>;
type EntityArrayResponseType = HttpResponse<IColeta[]>;

@Injectable({ providedIn: 'root' })
export class ColetaService {
  public resourceUrl = SERVER_API_URL + 'api/coletas';

  constructor(protected http: HttpClient) {}

  create(coleta: IColeta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(coleta);
    return this.http
      .post<IColeta>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(coleta: IColeta): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(coleta);
    return this.http
      .put<IColeta>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IColeta>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IColeta[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(coleta: IColeta): IColeta {
    const copy: IColeta = Object.assign({}, coleta, {
      dataColeta: coleta.dataColeta != null && coleta.dataColeta.isValid() ? coleta.dataColeta.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataColeta = res.body.dataColeta != null ? moment(res.body.dataColeta) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((coleta: IColeta) => {
        coleta.dataColeta = coleta.dataColeta != null ? moment(coleta.dataColeta) : null;
      });
    }
    return res;
  }
}
