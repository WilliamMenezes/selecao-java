import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IColeta, Coleta } from 'app/shared/model/coleta.model';
import { ColetaService } from './coleta.service';
import { IRevenda } from 'app/shared/model/revenda.model';
import { RevendaService } from 'app/entities/revenda';

@Component({
  selector: 'jhi-coleta-update',
  templateUrl: './coleta-update.component.html'
})
export class ColetaUpdateComponent implements OnInit {
  coleta: IColeta;
  isSaving: boolean;

  revendas: IRevenda[];
  dataColetaDp: any;

  editForm = this.fb.group({
    id: [],
    dataColeta: [null, [Validators.required]],
    unidadeMedida: [null, [Validators.required]],
    valorCompra: [null, [Validators.required]],
    valorVenda: [null, [Validators.required]],
    revenda: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected coletaService: ColetaService,
    protected revendaService: RevendaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ coleta }) => {
      this.updateForm(coleta);
      this.coleta = coleta;
    });
    this.revendaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRevenda[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRevenda[]>) => response.body)
      )
      .subscribe((res: IRevenda[]) => (this.revendas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(coleta: IColeta) {
    this.editForm.patchValue({
      id: coleta.id,
      dataColeta: coleta.dataColeta,
      unidadeMedida: coleta.unidadeMedida,
      valorCompra: coleta.valorCompra,
      valorVenda: coleta.valorVenda,
      revenda: coleta.revenda
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const coleta = this.createFromForm();
    if (coleta.id !== undefined) {
      this.subscribeToSaveResponse(this.coletaService.update(coleta));
    } else {
      this.subscribeToSaveResponse(this.coletaService.create(coleta));
    }
  }

  private createFromForm(): IColeta {
    const entity = {
      ...new Coleta(),
      id: this.editForm.get(['id']).value,
      dataColeta: this.editForm.get(['dataColeta']).value,
      unidadeMedida: this.editForm.get(['unidadeMedida']).value,
      valorCompra: this.editForm.get(['valorCompra']).value,
      valorVenda: this.editForm.get(['valorVenda']).value,
      revenda: this.editForm.get(['revenda']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IColeta>>) {
    result.subscribe((res: HttpResponse<IColeta>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRevendaById(index: number, item: IRevenda) {
    return item.id;
  }
}
