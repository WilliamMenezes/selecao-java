import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Coleta } from 'app/shared/model/coleta.model';
import { ColetaService } from './coleta.service';
import { ColetaComponent } from './coleta.component';
import { ColetaDetailComponent } from './coleta-detail.component';
import { ColetaUpdateComponent } from './coleta-update.component';
import { ColetaDeletePopupComponent } from './coleta-delete-dialog.component';
import { IColeta } from 'app/shared/model/coleta.model';

@Injectable({ providedIn: 'root' })
export class ColetaResolve implements Resolve<IColeta> {
  constructor(private service: ColetaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IColeta> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Coleta>) => response.ok),
        map((coleta: HttpResponse<Coleta>) => coleta.body)
      );
    }
    return of(new Coleta());
  }
}

export const coletaRoute: Routes = [
  {
    path: '',
    component: ColetaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'indraApp.coleta.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ColetaDetailComponent,
    resolve: {
      coleta: ColetaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.coleta.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ColetaUpdateComponent,
    resolve: {
      coleta: ColetaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.coleta.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ColetaUpdateComponent,
    resolve: {
      coleta: ColetaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.coleta.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const coletaPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ColetaDeletePopupComponent,
    resolve: {
      coleta: ColetaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'indraApp.coleta.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
