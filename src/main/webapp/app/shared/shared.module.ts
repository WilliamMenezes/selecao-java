import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IndraSharedLibsModule, IndraSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [IndraSharedLibsModule, IndraSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [IndraSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IndraSharedModule {
  static forRoot() {
    return {
      ngModule: IndraSharedModule
    };
  }
}
