import { IRevenda } from 'app/shared/model/revenda.model';

export interface IRegiao {
  id?: number;
  sigla?: string;
  estado?: string;
  municipio?: string;
  revendas?: IRevenda[];
}

export class Regiao implements IRegiao {
  constructor(public id?: number, public sigla?: string, public estado?: string, public municipio?: string, public revendas?: IRevenda[]) {}
}
