import { IRegiao } from 'app/shared/model/regiao.model';
import { IColeta } from 'app/shared/model/coleta.model';

export interface IRevenda {
  id?: number;
  nome?: string;
  codigoRevenda?: number;
  bandeira?: string;
  produto?: string;
  regiao?: IRegiao;
  coletas?: IColeta[];
}

export class Revenda implements IRevenda {
  constructor(
    public id?: number,
    public nome?: string,
    public codigoRevenda?: number,
    public bandeira?: string,
    public produto?: string,
    public regiao?: IRegiao,
    public coletas?: IColeta[]
  ) {}
}
