export interface IProduto {
  id?: number;
}

export class Produto implements IProduto {
  constructor(public id?: number) {}
}
