import { Moment } from 'moment';
import { IRevenda } from 'app/shared/model/revenda.model';

export interface IColeta {
  id?: number;
  dataColeta?: Moment;
  unidadeMedida?: string;
  valorCompra?: number;
  valorVenda?: number;
  revenda?: IRevenda;
}

export class Coleta implements IColeta {
  constructor(
    public id?: number,
    public dataColeta?: Moment,
    public unidadeMedida?: string,
    public valorCompra?: number,
    public valorVenda?: number,
    public revenda?: IRevenda
  ) {}
}
