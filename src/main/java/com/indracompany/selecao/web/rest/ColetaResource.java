package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.domain.Coleta;
import com.indracompany.selecao.service.ColetaService;
import com.indracompany.selecao.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.indracompany.selecao.domain.Coleta}.
 */
@RestController
@RequestMapping("/api")
public class ColetaResource {

    private final Logger log = LoggerFactory.getLogger(ColetaResource.class);

    private static final String ENTITY_NAME = "coleta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ColetaService coletaService;

    public ColetaResource(ColetaService coletaService) {
        this.coletaService = coletaService;
    }

    /**
     * {@code POST  /coletas} : Create a new coleta.
     *
     * @param coleta the coleta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coleta, or with status {@code 400 (Bad Request)} if the coleta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coletas")
    public ResponseEntity<Coleta> createColeta(@Valid @RequestBody Coleta coleta) throws URISyntaxException {
        log.debug("REST request to save Coleta : {}", coleta);
        if (coleta.getId() != null) {
            throw new BadRequestAlertException("A new coleta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Coleta result = coletaService.save(coleta);
        return ResponseEntity.created(new URI("/api/coletas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coletas} : Updates an existing coleta.
     *
     * @param coleta the coleta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coleta,
     * or with status {@code 400 (Bad Request)} if the coleta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coleta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coletas")
    public ResponseEntity<Coleta> updateColeta(@Valid @RequestBody Coleta coleta) throws URISyntaxException {
        log.debug("REST request to update Coleta : {}", coleta);
        if (coleta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Coleta result = coletaService.save(coleta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coleta.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /coletas} : get all the coletas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coletas in body.
     */
    @GetMapping("/coletas")
    public ResponseEntity<List<Coleta>> getAllColetas(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Coletas");
        Page<Coleta> page = coletaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /coletas/:id} : get the "id" coleta.
     *
     * @param id the id of the coleta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coleta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coletas/{id}")
    public ResponseEntity<Coleta> getColeta(@PathVariable Long id) {
        log.debug("REST request to get Coleta : {}", id);
        Optional<Coleta> coleta = coletaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coleta);
    }

    /**
     * {@code DELETE  /coletas/:id} : delete the "id" coleta.
     *
     * @param id the id of the coleta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coletas/{id}")
    public ResponseEntity<Void> deleteColeta(@PathVariable Long id) {
        log.debug("REST request to delete Coleta : {}", id);
        coletaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
