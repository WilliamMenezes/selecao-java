package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.domain.Coleta;
import com.indracompany.selecao.domain.Regiao;
import com.indracompany.selecao.domain.Revenda;
import com.indracompany.selecao.service.ColetaService;
import com.indracompany.selecao.service.RegiaoService;
import com.indracompany.selecao.service.RevendaService;
import com.indracompany.selecao.web.rest.vm.MediaVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * MediaResource controller
 */
@RestController
@RequestMapping("/api/media")
public class MediaResource {

    private final Logger log = LoggerFactory.getLogger(MediaResource.class);

    @Autowired
    private ColetaService coletaService;

    @Autowired
    private RegiaoService regiaoService;


    @Autowired
    private RevendaService revendaService;


    /**
     * GET getMedia
     */
    @GetMapping("/get-media")
    public String getMedia() {
        return "getMedia";
    }

    @PostMapping("/find-media")
    @Transactional
    public ResponseEntity<MediaVM.Media> findMedia(@PageableDefault(size = 2000) Pageable pageable, @Valid @RequestBody MediaVM mediaVM) {

        String municipio = mediaVM.getNomeMunicipio();
        Page<Regiao> regioes = regiaoService.findAll(pageable);
        List<BigDecimal> precos = new ArrayList<>();


        //percorre todas as regioes
        regioes.forEach(regiao ->
        {
            //ao encontrar o municipio
            if (regiao.getMunicipio().equalsIgnoreCase(municipio))
            {

                regiao.getRevendas().forEach(revenda ->
                {
                    revenda.getColetas().forEach(coleta ->
                    {
                        precos.add(coleta.getValorVenda());
                    });

                });

            }

        });


        //calcula a  média de preço
        Double average = precos.stream().mapToDouble(BigDecimal::doubleValue).average().orElse(0.0);
        String result = String.format("%.2f", average);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("media", result);
        return new ResponseEntity<>(new MediaVM.Media(municipio, result), httpHeaders, HttpStatus.OK);
    }



}
