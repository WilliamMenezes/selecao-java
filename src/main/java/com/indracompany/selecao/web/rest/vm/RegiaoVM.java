package com.indracompany.selecao.web.rest.vm;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class RegiaoVM {



    @NotNull
    @Size(min = 1, max = 2)
    private String sigla;


    @Override
    public String toString() {
        return "RegiaoVM{" +
            "sigla='" + sigla + '\'' +
            '}';
    }
    @Getter
    @Setter
    public static
    class Media
    {
        String municipio;
        String media;
        public Media(String mun, String v)
        {
            this.municipio=mun;
            this.media=v;

        }

    }

}


