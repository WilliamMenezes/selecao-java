package com.indracompany.selecao.web.rest;

import com.indracompany.selecao.domain.Revenda;
import com.indracompany.selecao.service.RevendaService;
import com.indracompany.selecao.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.indracompany.selecao.domain.Revenda}.
 */
@RestController
@RequestMapping("/api")
public class RevendaResource {

    private final Logger log = LoggerFactory.getLogger(RevendaResource.class);

    private static final String ENTITY_NAME = "revenda";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RevendaService revendaService;

    public RevendaResource(RevendaService revendaService) {
        this.revendaService = revendaService;
    }

    /**
     * {@code POST  /revendas} : Create a new revenda.
     *
     * @param revenda the revenda to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new revenda, or with status {@code 400 (Bad Request)} if the revenda has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/revendas")
    public ResponseEntity<Revenda> createRevenda(@Valid @RequestBody Revenda revenda) throws URISyntaxException {
        log.debug("REST request to save Revenda : {}", revenda);
        if (revenda.getId() != null) {
            throw new BadRequestAlertException("A new revenda cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Revenda result = revendaService.save(revenda);
        return ResponseEntity.created(new URI("/api/revendas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /revendas} : Updates an existing revenda.
     *
     * @param revenda the revenda to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated revenda,
     * or with status {@code 400 (Bad Request)} if the revenda is not valid,
     * or with status {@code 500 (Internal Server Error)} if the revenda couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/revendas")
    public ResponseEntity<Revenda> updateRevenda(@Valid @RequestBody Revenda revenda) throws URISyntaxException {
        log.debug("REST request to update Revenda : {}", revenda);
        if (revenda.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Revenda result = revendaService.save(revenda);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, revenda.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /revendas} : get all the revendas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of revendas in body.
     */
    @GetMapping("/revendas")
    public ResponseEntity<List<Revenda>> getAllRevendas(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Revendas");
        Page<Revenda> page = revendaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /revendas/:id} : get the "id" revenda.
     *
     * @param id the id of the revenda to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the revenda, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/revendas/{id}")
    public ResponseEntity<Revenda> getRevenda(@PathVariable Long id) {
        log.debug("REST request to get Revenda : {}", id);
        Optional<Revenda> revenda = revendaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(revenda);
    }

    /**
     * {@code DELETE  /revendas/:id} : delete the "id" revenda.
     *
     * @param id the id of the revenda to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/revendas/{id}")
    public ResponseEntity<Void> deleteRevenda(@PathVariable Long id) {
        log.debug("REST request to delete Revenda : {}", id);
        revendaService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
