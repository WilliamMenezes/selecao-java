package com.indracompany.selecao.web.rest.vm;



import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
public class MediaVM {



    @NotNull
    @Size(min = 1, max = 50)
    private String nomeMunicipio;


    @Override
    public String toString() {
        return "MediaVM{" +
            "municipio='" + nomeMunicipio + '\'' +
            '}';
    }
    @Getter
    @Setter
    public static
    class Media
    {
        String municipio;
        String media;
        public Media(String mun, String v)
        {
            this.municipio=mun;
            this.media=v;

        }

    }

}


