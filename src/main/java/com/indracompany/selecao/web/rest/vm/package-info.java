/**
 * View Models used by Spring MVC REST controllers.
 */
package com.indracompany.selecao.web.rest.vm;
