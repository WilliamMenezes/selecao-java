package com.indracompany.selecao.service;

import com.indracompany.selecao.domain.Regiao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Regiao}.
 */
public interface RegiaoService {

    /**
     * Save a regiao.
     *
     * @param regiao the entity to save.
     * @return the persisted entity.
     */
    Regiao save(Regiao regiao);

    /**
     * Get all the regiaos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Regiao> findAll(Pageable pageable);


    /**
     * Get the "id" regiao.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Regiao> findOne(Long id);

    /**
     * Delete the "id" regiao.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
