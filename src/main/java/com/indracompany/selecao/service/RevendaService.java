package com.indracompany.selecao.service;

import com.indracompany.selecao.domain.Revenda;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Revenda}.
 */
public interface RevendaService {

    /**
     * Save a revenda.
     *
     * @param revenda the entity to save.
     * @return the persisted entity.
     */
    Revenda save(Revenda revenda);

    /**
     * Get all the revendas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Revenda> findAll(Pageable pageable);


    /**
     * Get the "id" revenda.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Revenda> findOne(Long id);

    /**
     * Delete the "id" revenda.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
