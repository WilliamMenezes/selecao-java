package com.indracompany.selecao.service.impl;

import com.indracompany.selecao.service.RevendaService;
import com.indracompany.selecao.domain.Revenda;
import com.indracompany.selecao.repository.RevendaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Revenda}.
 */
@Service
@Transactional
public class RevendaServiceImpl implements RevendaService {

    private final Logger log = LoggerFactory.getLogger(RevendaServiceImpl.class);

    private final RevendaRepository revendaRepository;

    public RevendaServiceImpl(RevendaRepository revendaRepository) {
        this.revendaRepository = revendaRepository;
    }

    /**
     * Save a revenda.
     *
     * @param revenda the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Revenda save(Revenda revenda) {
        log.debug("Request to save Revenda : {}", revenda);
        return revendaRepository.save(revenda);
    }

    /**
     * Get all the revendas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Revenda> findAll(Pageable pageable) {
        log.debug("Request to get all Revendas");
        return revendaRepository.findAll(pageable);
    }


    /**
     * Get one revenda by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Revenda> findOne(Long id) {
        log.debug("Request to get Revenda : {}", id);
        return revendaRepository.findById(id);
    }

    /**
     * Delete the revenda by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Revenda : {}", id);
        revendaRepository.deleteById(id);
    }
}
