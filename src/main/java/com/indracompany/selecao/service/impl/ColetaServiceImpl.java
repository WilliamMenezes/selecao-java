package com.indracompany.selecao.service.impl;

import com.indracompany.selecao.service.ColetaService;
import com.indracompany.selecao.domain.Coleta;
import com.indracompany.selecao.repository.ColetaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Coleta}.
 */
@Service
@Transactional
public class ColetaServiceImpl implements ColetaService {

    private final Logger log = LoggerFactory.getLogger(ColetaServiceImpl.class);

    private final ColetaRepository coletaRepository;

    public ColetaServiceImpl(ColetaRepository coletaRepository) {
        this.coletaRepository = coletaRepository;
    }

    /**
     * Save a coleta.
     *
     * @param coleta the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Coleta save(Coleta coleta) {
        log.debug("Request to save Coleta : {}", coleta);
        return coletaRepository.save(coleta);
    }

    /**
     * Get all the coletas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Coleta> findAll(Pageable pageable) {
        log.debug("Request to get all Coletas");
        return coletaRepository.findAll(pageable);
    }


    /**
     * Get one coleta by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Coleta> findOne(Long id) {
        log.debug("Request to get Coleta : {}", id);
        return coletaRepository.findById(id);
    }

    /**
     * Delete the coleta by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Coleta : {}", id);
        coletaRepository.deleteById(id);
    }
}
