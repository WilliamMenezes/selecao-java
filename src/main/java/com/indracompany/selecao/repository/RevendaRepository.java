package com.indracompany.selecao.repository;

import com.indracompany.selecao.domain.Revenda;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Revenda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RevendaRepository extends JpaRepository<Revenda, Long> {

}
