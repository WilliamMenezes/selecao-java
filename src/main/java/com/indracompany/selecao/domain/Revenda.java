package com.indracompany.selecao.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Revenda entity.
 * @author William Menezes.
 */
@ApiModel(description = "Revenda entity. @author William Menezes.")
@Entity
@Table(name = "revenda")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Revenda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "codigo_revenda", nullable = false)
    private Integer codigoRevenda;

    @NotNull
    @Column(name = "bandeira", nullable = false)
    private String bandeira;

    @NotNull
    @Column(name = "produto", nullable = false)
    private String produto;

    @ManyToOne
    @JsonIgnoreProperties("revendas")
    private Regiao regiao;

    @OneToMany(mappedBy = "revenda")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Coleta> coletas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Revenda nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCodigoRevenda() {
        return codigoRevenda;
    }

    public Revenda codigoRevenda(Integer codigoRevenda) {
        this.codigoRevenda = codigoRevenda;
        return this;
    }

    public void setCodigoRevenda(Integer codigoRevenda) {
        this.codigoRevenda = codigoRevenda;
    }

    public String getBandeira() {
        return bandeira;
    }

    public Revenda bandeira(String bandeira) {
        this.bandeira = bandeira;
        return this;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public String getProduto() {
        return produto;
    }

    public Revenda produto(String produto) {
        this.produto = produto;
        return this;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public Regiao getRegiao() {
        return regiao;
    }

    public Revenda regiao(Regiao regiao) {
        this.regiao = regiao;
        return this;
    }

    public void setRegiao(Regiao regiao) {
        this.regiao = regiao;
    }

    public Set<Coleta> getColetas() {
        return coletas;
    }

    public Revenda coletas(Set<Coleta> coletas) {
        this.coletas = coletas;
        return this;
    }

    public Revenda addColeta(Coleta coleta) {
        this.coletas.add(coleta);
        coleta.setRevenda(this);
        return this;
    }

    public Revenda removeColeta(Coleta coleta) {
        this.coletas.remove(coleta);
        coleta.setRevenda(null);
        return this;
    }

    public void setColetas(Set<Coleta> coletas) {
        this.coletas = coletas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Revenda)) {
            return false;
        }
        return id != null && id.equals(((Revenda) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Revenda{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", codigoRevenda=" + getCodigoRevenda() +
            ", bandeira='" + getBandeira() + "'" +
            ", produto='" + getProduto() + "'" +
            "}";
    }
}
