package com.indracompany.selecao.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Coleta entity.
 * @author William Menezes.
 */
@ApiModel(description = "Coleta entity. @author William Menezes.")
@Entity
@Table(name = "coleta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Coleta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "data_coleta", nullable = false)
    private LocalDate dataColeta;

    @NotNull
    @Column(name = "unidade_medida", nullable = false)
    private String unidadeMedida;

    @NotNull
    @Column(name = "valor_compra", precision = 21, scale = 2, nullable = false)
    private BigDecimal valorCompra;

    @NotNull
    @Column(name = "valor_venda", precision = 21, scale = 2, nullable = false)
    private BigDecimal valorVenda;

    @ManyToOne
    @JsonIgnoreProperties("coletas")
    private Revenda revenda;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDataColeta() {
        return dataColeta;
    }

    public Coleta dataColeta(LocalDate dataColeta) {
        this.dataColeta = dataColeta;
        return this;
    }

    public void setDataColeta(LocalDate dataColeta) {
        this.dataColeta = dataColeta;
    }

    public String getUnidadeMedida() {
        return unidadeMedida;
    }

    public Coleta unidadeMedida(String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
        return this;
    }

    public void setUnidadeMedida(String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }

    public BigDecimal getValorCompra() {
        return valorCompra;
    }

    public Coleta valorCompra(BigDecimal valorCompra) {
        this.valorCompra = valorCompra;
        return this;
    }

    public void setValorCompra(BigDecimal valorCompra) {
        this.valorCompra = valorCompra;
    }

    public BigDecimal getValorVenda() {
        return valorVenda;
    }

    public Coleta valorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
        return this;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    public Revenda getRevenda() {
        return revenda;
    }

    public Coleta revenda(Revenda revenda) {
        this.revenda = revenda;
        return this;
    }

    public void setRevenda(Revenda revenda) {
        this.revenda = revenda;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coleta)) {
            return false;
        }
        return id != null && id.equals(((Coleta) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Coleta{" +
            "id=" + getId() +
            ", dataColeta='" + getDataColeta() + "'" +
            ", unidadeMedida='" + getUnidadeMedida() + "'" +
            ", valorCompra=" + getValorCompra() +
            ", valorVenda=" + getValorVenda() +
            "}";
    }
}
