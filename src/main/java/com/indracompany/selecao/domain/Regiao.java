package com.indracompany.selecao.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Regiao entity.
 * @author William Menezes.
 */
@ApiModel(description = "Regiao entity. @author William Menezes.")
@Entity
@Table(name = "regiao")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Regiao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 2)
    @Column(name = "sigla", length = 2, nullable = false)
    private String sigla;

    @NotNull
    @Size(max = 20)
    @Column(name = "estado", length = 20, nullable = false)
    private String estado;

    @NotNull
    @Size(max = 50)
    @Column(name = "municipio", length = 50, nullable = false)
    private String municipio;

    @OneToMany(mappedBy = "regiao")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Revenda> revendas = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public Regiao sigla(String sigla) {
        this.sigla = sigla;
        return this;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getEstado() {
        return estado;
    }

    public Regiao estado(String estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public Regiao municipio(String municipio) {
        this.municipio = municipio;
        return this;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Set<Revenda> getRevendas() {
        return revendas;
    }

    public Regiao revendas(Set<Revenda> revendas) {
        this.revendas = revendas;
        return this;
    }

    public Regiao addRevenda(Revenda revenda) {
        this.revendas.add(revenda);
        revenda.setRegiao(this);
        return this;
    }

    public Regiao removeRevenda(Revenda revenda) {
        this.revendas.remove(revenda);
        revenda.setRegiao(null);
        return this;
    }

    public void setRevendas(Set<Revenda> revendas) {
        this.revendas = revendas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Regiao)) {
            return false;
        }
        return id != null && id.equals(((Regiao) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Regiao{" +
            "id=" + getId() +
            ", sigla='" + getSigla() + "'" +
            ", estado='" + getEstado() + "'" +
            ", municipio='" + getMunicipio() + "'" +
            "}";
    }
}
