# Indra

Passos para execuçãoo do projeto:

1. instalar o node [Node.js][]:

```
    npm install
```



2 - Rodar o comando para inicializar o banco, subir o projeto
 e  compilar o webpack via Maven Wrapper

    ./mvnw

aplicacao subirá na porta 8080.

### Acesso

http://localhost:8080

Conta => Entrar

Credenciais: admin/admin
###Swagger:

Administracao => API

### Importante:

1 - Acessar o console h2: http://localhost:8080/h2-console
e executar as queries do script: 

```
src/main/resources/config/liquibase/changelog/db/massa.sql
```

2 - Acessar o front e realizar operaces CRUD

3 - Acessar o front e realizar operaces API (Swagger)

### Requests:

Ja existem Algumas requisições prontas e que podem ser chamadas fora do Front:

(RestClient ou Postman)

```
/requests/requests.http
```

:)
